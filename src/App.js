import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

/*
Display a list of movies where each movie contains a list of users that favorited it.

For detailed instructions, refer to instructions.md.
*/

const profiles = [
  {
    id: 1,
    userID: '1',
    favoriteMovieID: '1',
  },
  {
    id: 2,
    userID: '2',
    favoriteMovieID: '1',
  },
  {
    id: 3,
    userID: '4',
    favoriteMovieID: '5',
  },
  {
    id: 4,
    userID: '5',
    favoriteMovieID: '2',
  },
  {
    id: 5,
    userID: '3',
    favoriteMovieID: '5',
  },
  {
    id: 6,
    userID: '6',
    favoriteMovieID: '4',
  },
];

const users = {
  1: {
    id: 1,
    name: 'Jane Jones',
    userName: 'coder',
  },
  2: {
    id: 2,
    name: 'Matthew Johnson',
    userName: 'mpage',
  },
  3: {
    id: 3,
    name: 'Autumn Green',
    userName: 'user123',
  },
  4: {
    id: 4,
    name: 'John Doe',
    userName: 'user123',
  },
  5: {
    id: 5,
    name: 'Lauren Carlson',
    userName: 'user123',
  },
  6: {
    id: 6,
    name: 'Nicholas Lain',
    userName: 'user123',
  },
};

const movies = {
  1: {
    id: 1,
    name: 'Planet Earth',
  },
  2: {
    id: 2,
    name: 'Selma',
  },
  3: {
    id: 3,
    name: 'Million Dollar Baby',
  },
  4: {
    id: 4,
    name: 'Forrest Gump',
  },
  5: {
    id: 5,
    name: 'Get Out',
  },
};

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">ReactND - Coding Practice</h1>
        </header>
        <h2>How Popular is Your Favorite Movie?</h2>
		<FavoriteMovies />
      </div>
    );
  }
}

class FavoriteMovies extends Component {
  render() {
    let moviesClone = Object.assign({}, movies);
    moviesClone.length = 6
    let moviesArray = Array.from(moviesClone)
    moviesArray.shift()
    return (
      <div className="favorite-movies">
       {
          moviesArray.map((movie) => {
            let movieProfiles = profiles.filter((profile) => (
              +profile.favoriteMovieID === +movie.id
            ));

            let userList = [];
            movieProfiles.forEach((movieProfile) => {
              userList.push(users[movieProfile.userID]);
            });

      		console.log('movieProfiles', movieProfiles);
            return (
              <MovieInfo key={movie.id} movieName={movie.name} users={userList} />
              )
           }
         )
       }
      </div>
    )
  }
}

class MovieInfo extends Component {
  render() {
    return (
      <div className='movie-info'>
          <h2>{this.props.movieName}</h2>
          <p>Liked By:</p>
          <UserList users={this.props.users} />
      </div>
    )
  }
}

class UserList extends Component {
  render() {
    let listContent = null;

    if(this.props.users.length === 0){
      listContent = <p>None of the current users liked this movie</p>;
    } else {
      listContent = <ul>
       {
          this.props.users.map((user) => (
              <UserListItem key={user.id} user={user} />
          ))
       }
      </ul>
    }
    return (
      listContent
    )
  }
}

class UserListItem extends Component {
  render() {
    return (
      <li>
          {this.props.user.name} 
      </li>
    )
  }
}

export default App;
